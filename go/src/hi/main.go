package main

import (
	"fmt"
	"github.com/retep-mathwizard/utils/input"
)

func main() {
	name := input.StringInput(" > ")
	fmt.Println("nice to meet you " + name + "!!")
}
