package main

import (
	"fmt"
	"github.com/skilstak/go/input"
	"math"
	"math/rand"
	"time"
)

func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}

type Player struct {
	Name    string
	Attack  int
	Health  int
	Agility int
}

func createAi(player Player) Player {
	hp := player.Health
	floathp := float64(hp)
	att := player.Attack
	floatatt := float64(att)
	aihp := random(hp, (hp * 2))
	floataihp := float64(aihp)
	aiagi := random(10, 100)
	turns := math.Ceil(floataihp / floatatt)
	//hp/aiatt = turns
	//hp = turns * aiatt
	//hp/turns = aiatt
	aiatt := int(math.Ceil(floathp / turns))
	Ai := Player{Name: "Evil Boss", Attack: aiatt, Health: aihp, Agility: aiagi}
	return Ai
}
func WhoGoesFirst(playagi int, aiagi int) bool {
	if playagi >= aiagi {
		return true
	}
	return false
}
func battle(Attacker Player, Defender Player) Player {
	Defender.Health -= Attacker.Attack
	fmt.Println(Defender.Name+" now has ", Defender.Health)
	return Defender
}
func WhoWon(ai Player, pl Player) {
	if ai.Health > pl.Health {
		fmt.Println(ai.Name + " Won!")
	} else {
		fmt.Println(pl.Name + " Won!")
	}
}
func evalDead(x int) bool {
	if x > 0 {
		return false
	}
	return true
}
func main() {
	Character := Player{Name: "Bob", Attack: 12, Health: 100, Agility: 15}
	Ai := createAi(Character)
	alive := false
	PlGoes1st := WhoGoesFirst(Character.Agility, Ai.Agility)
	if PlGoes1st {
		for alive == false {
			Ai = battle(Character, Ai)
			input.Ask("")
			if evalDead(Ai.Health) {
				break
			}
			Character = battle(Ai, Character)
			input.Ask("")
			if evalDead(Character.Health) {
				break
			}
		}
	} else {
		for {
			Character = battle(Ai, Character)
			input.Ask("")
			if evalDead(Character.Health) {
				break
			}
			Ai = battle(Character, Ai)
			input.Ask("")
			if evalDead(Ai.Health) {
				break
			}
		}
	}
	WhoWon(Ai, Character)

}
